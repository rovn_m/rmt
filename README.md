# README #
REST-server test project

### Requirements ###

* jdk > 1.7.x
* mvn > 3.x.x

### Set up ###

* `mvn install`



### REST API ###

* `GET:/accounts/all` - receive all accounts as XML
* `GET:/accounts/{id}` - receive account by id as XML
* `PUT:/accounts` - add new account (XML)
* `DELETE:/accounts/{id}` - delete account by id
* `POST:/accounts` - update or add new account (XML)
* `POST:/accounts/transfer/{from}/{to}/{sum}` - transfer money between accounts

### Features ###
* Writing the built-in autotests
* To in-memory store the concurrentHashMap is used.
* Synchronization happens only in implementing a transaction to transfer money.
* Maximum use of the concurrentHashMap to prevent the possibility of thread locks (thread-safe Iterator, putIfAbsent).
* The application can be run without a container `java -jar rmt-1.0-SNAPSHOT-jar-with-dependencies.jar`.

### Possible Improvements###
* Extend the functionality of the API capabilities
* Add asynchronous API in addition to the synchronized method of money transfer (use singlethreadexecutor as a table changes queue, for example)
* Use database as locks implementation level (remove synchronized method from synchronous API)
* Further improvement of the solution scalability


### Testing ###

Any REST-client application can be used to check the software interface. The following are queries from Postman-client.

![addNew.PNG](https://bitbucket.org/repo/zGboxd/images/3135573944-addNew.PNG)
![delById.PNG](https://bitbucket.org/repo/zGboxd/images/1383012449-delById.PNG)
![getAll.PNG](https://bitbucket.org/repo/zGboxd/images/4190108378-getAll.PNG)
![getById.PNG](https://bitbucket.org/repo/zGboxd/images/2341032431-getById.PNG)
![transferFromTo.PNG](https://bitbucket.org/repo/zGboxd/images/138824861-transferFromTo.PNG)
![updateOrAddNew.PNG](https://bitbucket.org/repo/zGboxd/images/750006168-updateOrAddNew.PNG)