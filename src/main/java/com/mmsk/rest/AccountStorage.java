package com.mmsk.rest;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mmsk on 21.02.2016.
 */
public class AccountStorage {
    private static final ConcurrentHashMap<Integer, Account> aMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<Integer, Account> getaMap() {
        return aMap;
    }
}
