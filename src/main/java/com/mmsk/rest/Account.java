package com.mmsk.rest;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mmsk on 21.02.2016.
 */
@XmlRootElement//(name = "account")
public class Account {
    private int id;
    private float balance;

    public Account() {
    }

    public Account(int id, float balance) {
        this.id = id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public float getBalance() {
        return balance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                '}';
    }
}
