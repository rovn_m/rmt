package com.mmsk.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mmsk on 21.02.2016.
 */

@Path("/accounts")
public class AccountService {
    private final ConcurrentHashMap<Integer, Account> aMap = AccountStorage.getaMap();

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_XML)
    public  List<Account> getAllAccounts() {
        List<Account> accounts = new ArrayList<>();
        for (Map.Entry<Integer, Account> entry : aMap.entrySet()) {
            accounts.add(entry.getValue());
        }
        return accounts;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Account getAccount(@PathParam("id") int id) {
            return aMap.get(id);//200-OK,204-null
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response putAccount(JAXBElement<Account> jaxbAccount) {
        Response res;
        Account a = jaxbAccount.getValue();
        Account oldValue = aMap.putIfAbsent(a.getId(), a);
        if (oldValue == null) {
            res = Response.status(201).entity("added").build();
        } else {
            res = Response.status(200).entity("already present").build();
        }
        return res;
    }

    @DELETE
    @Path("{id}")
    public void deleteAccount(@PathParam("id") int id) {
        Account c = aMap.remove(id); //204-OK,404-null
        if(c==null)
            throw new NotFoundException("No such Account.");
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response updateAccount(JAXBElement<Account> jaxbAccount) {
        Account a = jaxbAccount.getValue();
        aMap.put(a.getId(), a);
        return Response.status(201).entity("added").build();
    }

    @GET
    @Path("/transfer/{from}/{to}/{sum}")
    public Response transferFromTo(
            @PathParam("from") int from,
            @PathParam("to") int to,
            @PathParam("sum") float sum) {
        String req = "" + from + "/" + to + "/" + sum;
        Response res;
        synchronized (aMap) {
            if (aMap.containsKey(from)) {
                float fromBalance=aMap.get(from).getBalance();
                if(fromBalance>=sum){
                    if (aMap.containsKey(to)){
                        aMap.put(from,new Account(from,fromBalance-sum));
                        aMap.put(to,new Account(to,aMap.get(to).getBalance()+sum));
                        res=Response.status(200).entity("transferred " + req).build();
                    }
                    else {
                        res=Response.status(406).entity("to not absent").build();
                    }
                }
                else {
                    res=Response.status(406).entity("money is not enough").build();
                }
            }
            else {
                res=Response.status(406).entity("from not absent").build();
            }
        }
        return res;
    }
}
